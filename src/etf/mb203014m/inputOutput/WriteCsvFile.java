package etf.mb203014m.inputOutput;

import etf.mb203014m.objects.Meal;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteCsvFile {
    private FileWriter fileWriter;

    public WriteCsvFile(String location) {
        try {
            this.fileWriter = new FileWriter(location);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Can't open file: " + location);
            System.exit(-1);
        }
    }

    public void writeToFile(ArrayList<Meal> meals) {
        try {
            fileWriter.write("id,cuisine\n");
            for (Meal meal : meals) {
                fileWriter.write(meal.getId() + "," + meal.getCuisine() + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Can't write file: " + fileWriter);
            System.exit(-1);
        }
    }
}
