package etf.mb203014m.inputOutput;

import etf.mb203014m.objects.Meal;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadJsonData {
    private FileReader fileReader;

    public ReadJsonData(String location) {
        try {
            this.fileReader = new FileReader(location);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Can't open file: " + location);
            System.exit(-1);
        }
    }

    public ArrayList<Meal> getAllMeals(boolean trainData) {
        ArrayList<Meal> meals = new ArrayList<>();
        try {
            JSONArray jsonArray = (JSONArray) new JSONParser().parse(fileReader);
            for (Object object : jsonArray) {
                try {
                    meals.add(new Meal((JSONObject) object, trainData));
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println("Can't read this type of meal!");
                    System.exit(-1);
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            System.err.println("Can't parse/read file: " + fileReader);
            System.exit(-1);
        }
        return meals;
    }
}
