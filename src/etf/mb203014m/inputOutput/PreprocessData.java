package etf.mb203014m.inputOutput;

import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

public class PreprocessData {
    private static final ArrayList<String> REMOVE_WORDS = new ArrayList<>(Arrays.asList("red", "not", "king",
            "boneless", "crumbled", "chunky", "plain", "uncooked", "cooked", "precooked", "frozen", "fresh", "diced",
            "tip", "baby", "powdered", "powder", "canned", "chopped", "knorr", "cubed", "toasted", "split", ",",
            "flavored", "dried", "softboiled", "activ", "quick", "light", "teriyaki", "packed", "curlyleaf", "whole",
            "free", "nonfat", "lowfat", "low", "fatfree", "less", "skinless", "broilerfryer", "glutenfree", "rings",
            "sheeps", "sargento", "traditional", "shredded", "extra", "sharp", "smoked", "curry guy", "new",
            "green giant", "mix", "mild", "quickcooking", "other", "kitchen", "reduced", "leaf", "kraft", "zesty",
            "sliced", "klondike", "gourmet", "reduc", "campbells", "single", "of", "refrigerated", "dry", "organic",
            "simply", "valley", "mountain", "high", "ground", "style", "jonshonville", "cajun", "smooth", "natural",
            "in", "all", "cold", "coldsmoked", "skim", "part", "cube", "firmli", "firmly", "breaded", "minced", "cooking",
            "hidden", "angel", "food", "blanched", "sanding", "dinner", "boiling", "condensed", "mixture", "puffed",
            "bones", "bitter", "golden", "purpose", "devils", "mccormick", "taco", "yoplait", "key", "evaporated",
            "arthur", "franks", "crushed", "diet", "fully", "baileys", "homemade", "johnsonville", "bone", "clotted",
            "leftover", "unsweetened", "wishbone®", "robusto", "inch", "thick", "holland", "house", "textured", "mini",
            "del", "monte", "passover", "cut", "up", "instant", "wide", "unsalted", "a", "an", "touch", "philadelphia",
            "rendered", "success", "hellmann", "best", "puff", "syd", "grilled", "old", "world", "veri", "color", "special",
            "crisco", "pure", "nusalt", "substitute", "foster", "farms", "dubliner", "boursin", "san", "marzano",
            "process", "mazola", "seasoned", "store", "bought", "selfrising", "vay", "pillsbury", "original", "pompeian",
            "added", "grated", "diamond", "crystal", "kikkoman", "small", "bramley", "ranch", "ortega", "swanson",
            "medium", "thousand", "island", "round", "pound", "prego", "spring", "into", "gold", "for", "land", "lakes",
            "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
            "x", "y", "z", "oz", "aged", "allpurpose", "slices", "cook", "drain", "half", "any", "concentrate",
            "crispcooked", "short", "bertolli", "baking", "bottled", "extract", "links", "brewed", "brinecured", "flavor",
            "preserves", "base", "chop", "classic", "classico", "sections", "fine", "compressed", "converted", "country",
            "plus", "spread", "creamed", "dipping", "distilled", "double", "fast", "rising", "active", "fashioned",
            "fatless", "fermented", "firm", "fried", "spreadable", "piece", "pieces", "good", "granular", "granulated",
            "guinness", "hard", "heavy", "spicy", "imitation", "jumbo", "large", "thawed", "undiluted", "family", "size",
            "lipton", "bags", "lower", "mashed", "mixed", "no salt", "calorie", "no", "pitted", "prepared", "refined",
            "regular", "convert", "side", "salted", "seedless", "semi", "semisweet", "on", "sodium", "stonefire",
            "straight", "sundried", "sweetened", "thin", "thickcut", "top", "unsmoked", "unseasoned", "unhulled",
            "unflavored", "unsulphured", "whipped", "and", "with"));
    public static final ConcurrentHashMap<String, Integer> mapIngredientToInteger = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, Integer> mapCuisineToInteger = new ConcurrentHashMap<>();
    private static final AtomicInteger index = new AtomicInteger(0);
    private static final AtomicInteger index2 = new AtomicInteger(0);

    public static void cleanData(ArrayList<Meal> meals) {
        meals.parallelStream().forEach(meal -> {
            ArrayList<String> ingredients = new ArrayList<>();
            meal.getIngredients().forEach(s -> {
                s = s.replaceAll("[-(),.'’/%&®™0123456789!]", "").toLowerCase();
                for (String word : REMOVE_WORDS) {
                    s = s.replaceAll("\\s*\\b" + word + "\\b\\s*", "");
                }
                s = s.replaceAll("\\s{2,}", " ").trim();
                if (!s.isEmpty()) {
                    if (s.contains(" or ")) {
                        String[] tempSplit = s.split(" or ");
                        ingredients.add(sortOrder(tempSplit[0]));
                        ingredients.add(sortOrder(tempSplit[1]));
                    } else {
                        ingredients.add(sortOrder(s));
                    }
                }
            });
            meal.setIngredients(ingredients);
        });
    }

    private static String sortOrder(String text) {
        ConcurrentSkipListSet<String> tempSet = new ConcurrentSkipListSet<>(Arrays.asList(text.split(" ")));
        StringBuilder stringBuilder = new StringBuilder();
        tempSet.forEach(stringBuilder::append);
        return stringBuilder.toString();
    }

    public static void turnToInt(ArrayList<Meal> meals) {
        meals.forEach(meal -> {
            meal.setIntIngredients(new ArrayList<>());
            meal.getIngredients().forEach(ingredient -> {
                if (!mapIngredientToInteger.containsKey(ingredient)) {
                    mapIngredientToInteger.put(ingredient, index.get());
                    index.incrementAndGet();
                }
                meal.getIntIngredients().add(mapIngredientToInteger.get(ingredient));
            });
            if (meal.getCuisine() != null) {
                if (!mapCuisineToInteger.containsKey(meal.getCuisine())) {
                    mapCuisineToInteger.put(meal.getCuisine(), index2.get());
                    index2.incrementAndGet();
                }
                meal.setCuisineNum(mapCuisineToInteger.get(meal.getCuisine()));
            }
        });
    }

    public static String findCuisine(int cuisineNum) {
        for (Map.Entry<String, Integer> entry : mapCuisineToInteger.entrySet()) {
            if (entry.getValue() == cuisineNum) return entry.getKey();
        }
        return null;
    }
}
