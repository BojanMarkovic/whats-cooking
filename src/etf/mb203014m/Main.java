package etf.mb203014m;

import etf.mb203014m.algorithms.KNNClassifier;
import etf.mb203014m.algorithms.MyAlgorithm;
import etf.mb203014m.algorithms.SimilarityDistanceAlgorithms;
import etf.mb203014m.algorithms.TfIdf;
import etf.mb203014m.algorithms.extraTree.ExtraTree;
import etf.mb203014m.inputOutput.PreprocessData;
import etf.mb203014m.inputOutput.ReadJsonData;
import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private static final int ALGORITHM = 0;

    public static void main(String[] args) {
        ReadJsonData readTrainJsonData = new ReadJsonData("train.json");
        ArrayList<Meal> trainMeals = readTrainJsonData.getAllMeals(true);

        ReadJsonData readTestJsonData = new ReadJsonData("test.json");
        ArrayList<Meal> testMeals = readTestJsonData.getAllMeals(false);

        getTrainStats(trainMeals);

        switch (ALGORITHM) {
            case 0: {
                TfIdf tfIdf = new TfIdf(1);
                tfIdf.executeAlgorithm(trainMeals, testMeals);
                tfIdf.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 1: {
                TfIdf tfIdf = new TfIdf(0);
                tfIdf.executeAlgorithm(trainMeals, testMeals);
                tfIdf.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 2: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                SimilarityDistanceAlgorithms similarityDistanceAlgorithms = new SimilarityDistanceAlgorithms(SimilarityDistanceAlgorithms.TYPE.LEVENSHTEIN_DISTANCE);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, testMeals);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 3: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                SimilarityDistanceAlgorithms similarityDistanceAlgorithms = new SimilarityDistanceAlgorithms(SimilarityDistanceAlgorithms.TYPE.OVERLAP_SIMILARITY);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, testMeals);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 4: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                SimilarityDistanceAlgorithms similarityDistanceAlgorithms = new SimilarityDistanceAlgorithms(SimilarityDistanceAlgorithms.TYPE.JACCARD_SIMILARITY);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, testMeals);
                similarityDistanceAlgorithms.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 5: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                KNNClassifier knnClassifier = new KNNClassifier(KNNClassifier.TYPE.JACCARD_SIMILARITY);
                knnClassifier.executeAlgorithm(trainMeals, testMeals);
                knnClassifier.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 6: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                KNNClassifier knnClassifier = new KNNClassifier(KNNClassifier.TYPE.LEVENSHTEIN_DISTANCE);
                knnClassifier.executeAlgorithm(trainMeals, testMeals);
                knnClassifier.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 7: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                KNNClassifier knnClassifier = new KNNClassifier(KNNClassifier.TYPE.OVERLAP_SIMILARITY);
                knnClassifier.executeAlgorithm(trainMeals, testMeals);
                knnClassifier.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 8: {
                PreprocessData.cleanData(trainMeals);
                PreprocessData.cleanData(testMeals);
                PreprocessData.turnToInt(trainMeals);
                PreprocessData.turnToInt(testMeals);
                ExtraTree extraTree = new ExtraTree();
                extraTree.executeAlgorithm(trainMeals, testMeals);
                extraTree.executeAlgorithm(trainMeals, trainMeals);
                break;
            }
            case 9: {
                MyAlgorithm myAlgorithm = new MyAlgorithm();
                myAlgorithm.executeAlgorithm(trainMeals, testMeals);
                break;
            }
        }
    }

    private static void getTrainStats(ArrayList<Meal> trainMeals) {
        System.out.println("**********Stats**********");
        HashMap<String, Integer> countCuisines = new HashMap<>();
        HashMap<String, Integer> countIngredientsInCuisines = new HashMap<>();
        trainMeals.forEach(meal -> {
            if (!countCuisines.containsKey(meal.getCuisine())) {
                countCuisines.put(meal.getCuisine(), 0);
                countIngredientsInCuisines.put(meal.getCuisine(), 0);
            }
            countCuisines.put(meal.getCuisine(), countCuisines.get(meal.getCuisine()) + 1);
            countIngredientsInCuisines.put(meal.getCuisine(), countIngredientsInCuisines.get(meal.getCuisine()) + meal.getIngredients().size());
        });
        System.out.println("Train data stats:");
        AtomicInteger counter = new AtomicInteger(0);
        countCuisines.forEach((s, i) -> {
            System.out.println("Cuisine: " + s + " number of times in train data: " + i);
            counter.getAndAdd(i);
        });
        System.out.println("Number of total cuisines: " + counter.get());
        counter.set(0);
        countIngredientsInCuisines.forEach((s, i) -> {
            System.out.println("Cuisine: " + s + " number of ingredients in cuisine in train data: " + i);
            counter.getAndAdd(i);
        });
        System.out.println("Number of total ingredients: " + counter.get());
        System.out.println("******************************************");
    }
}
