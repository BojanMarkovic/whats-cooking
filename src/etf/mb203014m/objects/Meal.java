package etf.mb203014m.objects;

import org.json.simple.JSONObject;

import java.util.ArrayList;

public class Meal {
    private final long id;
    private String cuisine;
    private ArrayList<String> ingredients;
    private ArrayList<Integer> intIngredients;
    private int cuisineNum;

    public Meal(JSONObject jsonObject, boolean trainData) {
        this.id = (long) jsonObject.get("id");
        if (trainData) {
            this.cuisine = (String) jsonObject.get("cuisine");
        }
        this.ingredients = (ArrayList<String>) jsonObject.get("ingredients");
    }

    public long getId() {
        return id;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<Integer> getIntIngredients() {
        return intIngredients;
    }

    public void setIntIngredients(ArrayList<Integer> intIngredients) {
        this.intIngredients = intIngredients;
    }

    public int getCuisineNum() {
        return cuisineNum;
    }

    public void setCuisineNum(int cuisineNum) {
        this.cuisineNum = cuisineNum;
    }
}
