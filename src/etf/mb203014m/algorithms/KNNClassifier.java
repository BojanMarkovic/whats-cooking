package etf.mb203014m.algorithms;

import etf.mb203014m.inputOutput.WriteCsvFile;
import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

public class KNNClassifier {
    private final String fileName;
    private final TYPE type;

    private static class Element implements Comparable<Element> {
        protected final Meal meal;
        protected final Double distance;

        public Element(Meal meal, Double distance) {
            this.meal = meal;
            this.distance = distance;
        }

        @Override
        public int compareTo(Element o) {
            return o.distance.compareTo(distance);
        }
    }

    public KNNClassifier(TYPE type) {
        this.fileName = "test_KNN" + type.name() + ".csv";
        this.type = type;
    }

    public void executeAlgorithm(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        try {
            System.out.println("Running kNN with " + type.name());
            AtomicLong errors = new AtomicLong(0);

            double k = 5;//Math.sqrt(trainData.size())
            testData.parallelStream().forEach(meal -> {
                TreeSet<Element> distances = new TreeSet<>();
                trainData.forEach(meal1 -> {
                    double temp = calculateDistance(meal, meal1);
                    if (temp != 0)
                        distances.add(new Element(meal1, temp));
                });
                HashMap<String, Integer> typesOfMeals = new HashMap<>();
                for (int i = 0; i < k; i++) {
                    if (distances.isEmpty()) break;
                    Element first;
                    if(type==TYPE.LEVENSHTEIN_DISTANCE){
                        first = distances.last();
                    }else {
                        first = distances.first();
                    }
                    String cuisine = first.meal.getCuisine();
                    distances.remove(first);
                    typesOfMeals.putIfAbsent(cuisine, 0);
                    typesOfMeals.put(cuisine, typesOfMeals.get(cuisine) + 1);
                }
                String cuisine = "NULL";
                if (!typesOfMeals.isEmpty()) {
                    cuisine = typesOfMeals.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();
                }
                if ((meal.getCuisine() != null) && (meal.getCuisine().compareToIgnoreCase(cuisine) != 0))
                    errors.incrementAndGet();
                meal.setCuisine(cuisine);
            });

            WriteCsvFile writeCsvFile = new WriteCsvFile(fileName);
            writeCsvFile.writeToFile(testData);
            System.out.println("Error rate if run on train data:" + errors.get());
        } catch (
                Exception e) {
            e.printStackTrace();
            System.err.println("Something happened!");
        }
    }

    private Double calculateDistance(Meal meal, Meal meal1) {
        double numSame = meal.getIngredients().stream().filter(ingredient -> meal1.getIngredients().contains(ingredient)).count();
        switch (type) {
            case LEVENSHTEIN_DISTANCE:
                return Math.abs(meal.getIngredients().size() - meal1.getIngredients().size()) + meal.getIngredients().size() - numSame;
            case OVERLAP_SIMILARITY:
                return numSame / Math.min(meal.getIngredients().size(), meal1.getIngredients().size());
            case JACCARD_SIMILARITY:
                return numSame / (meal.getIngredients().size() + meal1.getIngredients().size() - numSame);
            default:
                return -1.0;
        }
    }

    public enum TYPE {LEVENSHTEIN_DISTANCE, OVERLAP_SIMILARITY, JACCARD_SIMILARITY}
}
