package etf.mb203014m.algorithms;

import etf.mb203014m.algorithms.extraTree.DecisionTree;
import etf.mb203014m.inputOutput.PreprocessData;
import etf.mb203014m.inputOutput.WriteCsvFile;
import etf.mb203014m.objects.Meal;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

public class MyAlgorithm {
    private final AtomicLong errors = new AtomicLong(0);

    public void executeAlgorithm(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        try {
            System.out.println("Running my algorithm");
            ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> tfIdf = runTfIdf(trainData, testData);
            System.out.println("tfidf");
            PreprocessData.cleanData(trainData);
            PreprocessData.cleanData(testData);
            PreprocessData.turnToInt(trainData);
            PreprocessData.turnToInt(testData);
            ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> extraTree = extraTree(trainData, testData);
            System.out.println("done");
            tfIdf.forEach((mealId, resultHasMap) ->
                    resultHasMap.forEach((cuisine, value) ->resultHasMap.put(cuisine, value / 2 +
                                    extraTree.get(mealId).getOrDefault(cuisine, 0.0) / 2)));

            testData.forEach(meal -> {
                String key = Collections.max(tfIdf.get(meal.getId()).entrySet(), Map.Entry.comparingByValue()).getKey();
                if ((meal.getCuisine() != null) && (meal.getCuisine().compareToIgnoreCase(key) != 0))
                    errors.incrementAndGet();
                meal.setCuisine(key);
            });

            String fileName = "test_MyAlgorithm.csv";
            WriteCsvFile writeCsvFile = new WriteCsvFile(fileName);
            writeCsvFile.writeToFile(testData);
            System.out.println("Error rate if run on train data:" + errors.get());
        } catch (
                Exception e) {
            e.printStackTrace();
            System.err.println("Something happened!");
        }
    }

    private ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> extraTree(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        ConcurrentHashMap<Long, ConcurrentHashMap<Integer, Double>> result = new ConcurrentHashMap<>();

        IntStream.range(0, 3000).parallel().forEach(value -> {
            DecisionTree decisionTree = new DecisionTree(-1);
            decisionTree.generateDecisionTree(trainData);

            testData.forEach(meal -> {
                int temp = decisionTree.getCuisine(meal);
                if (temp != -1) {
                    result.putIfAbsent(meal.getId(), new ConcurrentHashMap<>());
                    result.get(meal.getId()).putIfAbsent(temp, 0.0);
                    result.get(meal.getId()).put(temp, result.get(meal.getId()).get(temp) + 1);
                }
            });
        });

        ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> tempMain = new ConcurrentHashMap<>();
        testData.parallelStream().forEach(meal -> {
            double sum = result.get(meal.getId()).values().stream().reduce(0.0, Double::sum);
            result.get(meal.getId()).forEach((cuisine, probability) -> result.get(meal.getId()).put(cuisine, result.get(meal.getId()).get(cuisine) / sum));
            tempMain.putIfAbsent(meal.getId(), turnCuisineToString(result.get(meal.getId())));
        });

        return tempMain;
    }

    private ConcurrentHashMap<String, Double> turnCuisineToString(ConcurrentHashMap<Integer, Double> result) {
        ConcurrentHashMap<String, Double> temp = new ConcurrentHashMap<>();
        result.forEach((cuisineInt, value) -> temp.put(PreprocessData.findCuisine(cuisineInt), value));
        return temp;
    }

    private ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> runTfIdf(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        ConcurrentHashMap<String, ConcurrentHashMap<String, Double>> knowledge = new ConcurrentHashMap<>();

        trainData.parallelStream().forEach(meal -> {
            knowledge.putIfAbsent(meal.getCuisine(), new ConcurrentHashMap<>());
            meal.getIngredients().forEach(ingredient -> {
                ConcurrentHashMap<String, Double> temp = knowledge.get(meal.getCuisine());
                temp.putIfAbsent(ingredient, 0.0);
                temp.put(ingredient, temp.get(ingredient) + 1.0);
            });
        });

        ConcurrentHashMap<String, Double> idfIngredients = new ConcurrentHashMap<>();
        knowledge.forEach((key, value) -> value.entrySet().parallelStream().forEach(ingredient -> {
            double temp = knowledge.entrySet().stream().filter(cuisine2 -> cuisine2.getValue().containsKey(ingredient.getKey())).count();
            idfIngredients.putIfAbsent(ingredient.getKey(), Math.log10(knowledge.size() / temp));
        }));

        knowledge.entrySet().parallelStream().forEach(cuisine -> cuisine.getValue().forEach((ingredient, value) ->
                    cuisine.getValue().put(ingredient, (1 + Math.log10(value) * idfIngredients.get(ingredient)))));

        ConcurrentHashMap<Long, ConcurrentHashMap<String, Double>> tempMain = new ConcurrentHashMap<>();
        testData.parallelStream().forEach(meal -> {
            ConcurrentHashMap<String, Double> temp = new ConcurrentHashMap<>();
            knowledge.forEach((cuisine, ingredients) -> {
                double probability = meal.getIngredients().stream().filter(ingredients::containsKey).mapToDouble(ingredients::get).sum();
                temp.put(cuisine, probability);
            });

            tempMain.putIfAbsent(meal.getId(), temp);
        });
        return tempMain;
    }
}
