package etf.mb203014m.algorithms.extraTree;

import etf.mb203014m.inputOutput.PreprocessData;
import etf.mb203014m.inputOutput.WriteCsvFile;
import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

public class ExtraTree {
    private final AtomicLong errors = new AtomicLong(0);

    public void executeAlgorithm(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        try {
            System.out.println("Running ExtraTree");
            ConcurrentHashMap<Long, ConcurrentHashMap<Integer, Integer>> result = new ConcurrentHashMap<>();

            IntStream.range(0, 3000).parallel().forEach(value -> {
                DecisionTree decisionTree = new DecisionTree(-1);
                decisionTree.generateDecisionTree(trainData);

                testData.forEach(meal -> {
                    int temp = decisionTree.getCuisine(meal);
                    if (temp != -1) {
                        result.putIfAbsent(meal.getId(), new ConcurrentHashMap<>());
                        result.get(meal.getId()).putIfAbsent(temp, 0);
                        result.get(meal.getId()).put(temp, result.get(meal.getId()).get(temp) + 1);
                    }
                });
            });

            testData.parallelStream().forEach(meal -> {
                String cuisine = null;
                if (result.get(meal.getId()).size() != 0) {
                    cuisine = PreprocessData.findCuisine(result.get(meal.getId()).entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey());
                }
                if (cuisine == null) cuisine = "";
                if ((meal.getCuisine() != null) && (meal.getCuisine().compareToIgnoreCase(cuisine) != 0))
                    errors.incrementAndGet();
                meal.setCuisine(cuisine);
            });

            String fileName = "test_ExtraTree.csv";
            WriteCsvFile writeCsvFile = new WriteCsvFile(fileName);
            writeCsvFile.writeToFile(testData);
            System.out.println("Error rate if run on train data:" + errors.get());
        } catch (
                Exception e) {
            e.printStackTrace();
            System.err.println("Something happened!");
        }
    }
}
