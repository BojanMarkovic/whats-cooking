package etf.mb203014m.algorithms.extraTree;

import etf.mb203014m.objects.Meal;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DecisionTree {
    private static class Node {
        private Node left, right;
        private final Integer ingredient;
        private final HashMap<Integer, Integer> cuisines = new HashMap<>();

        public Node(Integer ingredient) {
            this.ingredient = ingredient;
        }
    }

    private Node rootNode;
    private int numberOfInputData;
    private final SecureRandom secureRandom = new SecureRandom();

    public DecisionTree(int numberOfInputData) {
        this.numberOfInputData = numberOfInputData;
    }

    public void generateDecisionTree(ArrayList<Meal> trainData) {
        ArrayList<Meal> meals = extractData(trainData);
        while (!meals.isEmpty()) {
            Meal meal = meals.get(secureRandom.nextInt(meals.size()));
            meals.remove(meal);
            ArrayList<Integer> ingredients = new ArrayList<>(meal.getIntIngredients());
            if (rootNode == null) {
                rootNode = generateNewNode(ingredients);
            }
            Node lastNode = rootNode;
            while (!ingredients.isEmpty()) {
                lastNode.cuisines.putIfAbsent(meal.getCuisineNum(), 0);
                lastNode.cuisines.put(meal.getCuisineNum(), lastNode.cuisines.get(meal.getCuisineNum()) + 1);

                if (ingredients.contains(lastNode.ingredient)) {
                    ingredients.remove(lastNode.ingredient);
                    if (ingredients.isEmpty()) break;
                    if (lastNode.left == null) {
                        lastNode.left = generateNewNode(ingredients);
                    }
                    lastNode = lastNode.left;
                } else {
                    if (lastNode.right == null) {
                        lastNode.right = generateNewNode(ingredients);
                    }
                    lastNode = lastNode.right;
                }
            }
        }
    }

    private ArrayList<Meal> extractData(ArrayList<Meal> trainData) {
        if (numberOfInputData == -1) {
            return new ArrayList<>(trainData);
        } else {
            ArrayList<Meal> temp = new ArrayList<>();
            for (; numberOfInputData > -1; numberOfInputData--) {
                temp.add(trainData.get(secureRandom.nextInt(trainData.size())));
            }
            return temp;
        }
    }

    private Node generateNewNode(ArrayList<Integer> ingredients) {
        return new Node(ingredients.get(secureRandom.nextInt(ingredients.size())));
    }

    public Integer getCuisine(Meal meal) {
        Node temp = rootNode;
        while (true) {
            if (meal.getIntIngredients().contains(temp.ingredient)) {
                if (temp.left == null) break;
                temp = temp.left;
            } else {
                if (temp.right == null) break;
                temp = temp.right;
            }
        }
        return !temp.cuisines.isEmpty() ?
                temp.cuisines.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey() :
                Integer.valueOf(0);
    }
}
