package etf.mb203014m.algorithms;

import etf.mb203014m.inputOutput.WriteCsvFile;
import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class SimilarityDistanceAlgorithms {
    private final String fileName;
    private final TYPE type;

    public SimilarityDistanceAlgorithms(TYPE type) {
        this.fileName = "test_" + type.name() + ".csv";
        this.type = type;
    }

    public void executeAlgorithm(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        try {
            System.out.println("Running " + type.name());
            AtomicLong errors = new AtomicLong(0);

            testData.parallelStream().forEach(testMeal -> {
                AtomicReference<Double> minMax = new AtomicReference<>(Double.MIN_VALUE);
                if(type==TYPE.LEVENSHTEIN_DISTANCE){
                    minMax.set(Double.MAX_VALUE);
                }
                AtomicReference<Meal> similarMeal = new AtomicReference<>();
                trainData.forEach(trainMeal -> {
                    double numSame = testMeal.getIngredients().stream().filter(ingredient ->
                            trainMeal.getIngredients().contains(ingredient)).count();
                    switch (type) {
                        case LEVENSHTEIN_DISTANCE: {
                            double index = Math.abs(testMeal.getIngredients().size() - trainMeal.getIngredients().size()) + testMeal.getIngredients().size() - numSame;
                            if (index < minMax.get()) {
                                minMax.set(index);
                                similarMeal.set(trainMeal);
                            }
                            break;
                        }
                        case OVERLAP_SIMILARITY: {
                            double index = numSame / Math.min(testMeal.getIngredients().size(), trainMeal.getIngredients().size());
                            if (index > minMax.get()) {
                                minMax.set(index);
                                similarMeal.set(trainMeal);
                            }
                            break;
                        }
                        case JACCARD_SIMILARITY: {
                            double index = numSame / (testMeal.getIngredients().size() + trainMeal.getIngredients().size() - numSame);
                            if (index > minMax.get()) {
                                minMax.set(index);
                                similarMeal.set(trainMeal);
                            }
                            break;
                        }
                    }
                });
                if ((testMeal.getCuisine() != null) && (testMeal.getCuisine().compareToIgnoreCase(similarMeal.get().getCuisine()) != 0))
                    errors.incrementAndGet();
                testMeal.setCuisine(similarMeal.get().getCuisine());
            });


            WriteCsvFile writeCsvFile = new WriteCsvFile(fileName);
            writeCsvFile.writeToFile(testData);
            System.out.println("Error rate if run on train data:" + errors.get());
        } catch (
                Exception e) {
            e.printStackTrace();
            System.err.println("Something happened!");
        }
    }

    public enum TYPE {LEVENSHTEIN_DISTANCE, OVERLAP_SIMILARITY, JACCARD_SIMILARITY}
}
