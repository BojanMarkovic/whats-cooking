package etf.mb203014m.algorithms;

import etf.mb203014m.inputOutput.WriteCsvFile;
import etf.mb203014m.objects.Meal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TfIdf {
    private final int type;
    private final String fileName;

    public TfIdf(int type) {
        this.type = type;
        this.fileName = "test_TfIdf_Type" + type + ".csv";
    }

    public void executeAlgorithm(ArrayList<Meal> trainData, ArrayList<Meal> testData) {
        try {
            System.out.println("Running TfIdf, type:" + type);
            ConcurrentHashMap<String, ConcurrentHashMap<String, Double>> knowledge = new ConcurrentHashMap<>();
            AtomicLong errors = new AtomicLong(0);

            trainData.parallelStream().forEach(meal -> {
                knowledge.putIfAbsent(meal.getCuisine(), new ConcurrentHashMap<>());
                meal.getIngredients().forEach(ingredient -> {
                    ConcurrentHashMap<String, Double> temp = knowledge.get(meal.getCuisine());
                    temp.putIfAbsent(ingredient, 0.0);
                    temp.put(ingredient, temp.get(ingredient) + 1.0);
                });
            });

            ConcurrentHashMap<String, Double> idfIngredients = new ConcurrentHashMap<>();
            knowledge.forEach((key, value) -> value.entrySet().parallelStream().forEach(ingredient -> {
                double temp = knowledge.entrySet().stream().filter(cuisine2 -> cuisine2.getValue().containsKey(ingredient.getKey())).count();
                idfIngredients.putIfAbsent(ingredient.getKey(), Math.log10(knowledge.size() / temp));
            }));

            knowledge.entrySet().parallelStream().forEach(cuisine -> {
                double maxTfCuisine = (type == 0) ? cuisine.getValue().entrySet().stream().max(Map.Entry.comparingByValue()).get().getValue() : 0;
                cuisine.getValue().forEach((ingredient, value) -> {
                    switch (type) {
                        case 0:
                            cuisine.getValue().put(ingredient, (0.5 + 0.5 * value / maxTfCuisine) * idfIngredients.get(ingredient));
                            break;
                        case 1:
                            cuisine.getValue().put(ingredient, (1 + Math.log10(value) * idfIngredients.get(ingredient)));
                            break;
                    }
                });
            });

            testData.parallelStream().forEach(meal -> {
                HashMap<String, Double> temp = new HashMap<>();
                knowledge.forEach((cuisine, ingredients) -> {
                    double probability = meal.getIngredients().stream().filter(ingredients::containsKey).mapToDouble(ingredients::get).sum();
                    temp.put(cuisine, probability);
                });

                String key = Collections.max(temp.entrySet(), Map.Entry.comparingByValue()).getKey();
                if ((meal.getCuisine() != null) && (meal.getCuisine().compareToIgnoreCase(key) != 0))
                    errors.incrementAndGet();
                meal.setCuisine(key);
            });

            WriteCsvFile writeCsvFile = new WriteCsvFile(fileName);
            writeCsvFile.writeToFile(testData);
            System.out.println("Error rate if run on train data:" + errors.get());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Something happened!");
        }
    }
}
